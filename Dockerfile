FROM gittools/gitversion:5.6.7-alpine.3.12-x64-5.0

ENTRYPOINT '/bin/sh'

# RUN addgroup --gid $JENKINS_USER_ID $JENKINS_USER_NAME && \
#     adduser --disabled-password --uid $JENKINS_USER_ID --ingroup $JENKINS_USER_NAME $JENKINS_USER_NAME

# WORKDIR $JENKINS_WORKDIR

# USER $JENKINS_USER_NAME

RUN addgroup --gid 1001 jenkins && \
    adduser --disabled-password --uid 1001 --ingroup jenkins jenkins

WORKDIR /home/jenkins

USER 1001